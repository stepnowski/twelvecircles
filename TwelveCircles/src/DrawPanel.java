import java.awt.Graphics;
import javax.swing.JPanel;

public class DrawPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int width = getWidth();
		int height = getHeight();
		
		for(int s=1;s<=12;s++)
		{	
			g.drawOval(width/2-s*10, height/2-s*10, s*20, s*20);
		}
	}
}
